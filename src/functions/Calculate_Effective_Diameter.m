 function [Sample_name,Deff, effvar, ci_vol]=Calculate_Effective_Diameter(data_path,Sample_name,detritus)

    if nargin < 3 
        detritus = 0;
    end
    data = load([data_path Sample_name]);

    data=data.bindata;

    bin_diams = data(:,1); %Diam
    bin_heights = data(:,4); %number of cell by m3

    if detritus == 0
        data_path_save = [data_path 'deffs/'];
        mkdirIfNotExists(data_path_save);
    else
        data_path_save = [data_path 'minus_detritus_deffs/'];
        mkdirIfNotExists(data_path_save);
    end
    %%%%%%%%% REMOVE DETRITAL CONTRIBUTION %%%%%%%%%%%%%%%%%%%%%%%%
    %Assign Jungian slope S
    S=-4;

    D=bin_diams./1e6;
    N=bin_heights;
    V=4./3.*pi.*(D./2).^3.*N;

    %Find minimum in VSD data
    [Vmin I]=min(V(1:30));

    %Calculate k at Vmin
    k=N(I)./(D(I).^S);

    %Calculate full DSDs
    Nd=k.*D.^S;
    Vd=pi./6.*k.*D.^(S+3);

    %Construct new log spaced size vector for detrital extrapolation - 54 for 3.01, 19 for 1.16
    if bin_diams(1)<1.2
        binspace=19;
    elseif bin_diams(1)<3.1
        binspace=54;
    else binspace=60;
    end;

    smpsd=logspace(log10(D(1)),log10(0.7./1e6),binspace)';
    smpsd=flipud(smpsd);
    smpsd=smpsd(1:length(smpsd)-1);
    % smpsd Is used to increase the number of bin diam (from 0.7 to bin_diams(1))
    nupsd=[smpsd;D];
    nuNd=k.*nupsd.^S;
    dsd(:,1)=nupsd;
    dsd(:,2)=nuNd;
    nuVd=pi./6.*k.*nupsd.^(S+3);
    asd(:,1)=dsd(:,1);
    asd(1:binspace-1,2)=0;
    %the part that we add is null
    %Here we remove the detritus slope 
    asd(binspace:length(nupsd),2)=bin_heights-dsd(binspace:length(nupsd),2);

    %%%%%%%%% DETRITAL CONTRIBUTION REMOVED %%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%% TEST PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % %Plot Number and Volume size distributions
    %  fig1=figure; 
    %  set(fig1,'Name',strcat('PSD Sample :',Sample_name))
    %  subplot(2,1,1)
    %  loglog(D,N,'b.-'); hold on; grid
    %  loglog(D,Nd,'r.');
    %  loglog(D,N-Nd,'g.-');
    %  loglog(nupsd,nuNd,'r'); hold off
    % % 
    %  subplot(2,1,2)
    %  plot(D,V,'b.-'); hold on; grid
    %  plot(D,Vd,'r.');
    %  plot(D,V-Vd,'g.-');
    %  plot(nupsd,nuVd,'r'); hold off

    %%%%%%%%%%%%%% TEST PLOTS END %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if detritus == 0
        asd=[];
        asd(:,1) = bin_diams./1e6;
        asd(:,2) = bin_heights;
    end

    %Calculate log bins sizes And conversion in meter
    sdd=length(asd);
    orig_dd=diff(asd(:,1)); 
    orig_dd(sdd)=orig_dd(sdd-1);

    %Calculate size spectral density 
    % Here we divise by the difference to obtain the values by µm => (cells/m3/m)
    psd_sd=(1./orig_dd).*asd(:,2);

    %Construct new linear spaced size vector for density spectrum and all psd data
    tpsd(:,1)=[1:1:fix(max(asd(:,1).*1e6))]'; %.*1e6 for the fix (round 1)
    tpsd(:,1)=tpsd(:,1)./1e6;
    %Interpolate total psd spectral density onto linear size bins
    tpsd(:,2)=interp1(asd(:,1),psd_sd,tpsd(:,1),'linear');
    sdd=length(tpsd);
    %Remember psd data  in meter and m3

    psd(:,1)=tpsd(:,1);
    psd(:,2)=tpsd(:,2);

    %Remove all NAN and negative
    psd(find(psd<0))=0;
    psd(find(isnan(psd)))=0;
    %Generate meter based size distribution for integrating purposes
    PSDM(:,1)=psd(:,1);
    PSDM(:,2)=psd(:,2);
    %Calculate initial PSD integration parameters
    deltad=1; DELTADM=1;

    %*********************************************************************************
    %	Calculate PSD snynopsis
    %*********************************************************************************

    %Calculate F(d)d3d(d) for Ci calculations
    ci_vol=pi./6.*sum(PSDM(:,2).*(PSDM(:,1).^3).*DELTADM);


    %Effective radius of Hansen & Travis 1974 p 548. First calculate psd in radii
    radpsd(:,1)=psd(:,1)./2;
    radpsd(:,2)=psd(:,2);

    %Then find effective radius to plot
    effrad=sum(radpsd(:,1).*pi.*radpsd(:,1).^2.*radpsd(:,2))./(sum(pi.*radpsd(:,1).^2.*radpsd(:,2)));
    avrad=sum(radpsd(:,1).*pi.*radpsd(:,1).^2.75.*radpsd(:,2))./(sum(pi.*radpsd(:,1).^2.75.*radpsd(:,2)));
    %Also calculate effective variance as width of size distribution. First find geometrical mean G
    G=(sum(pi.*radpsd(:,1).^2.*radpsd(:,2))).*(radpsd(2,1)-radpsd(1,1));
    effvar=(1./((sum(pi.*radpsd(:,1).^2.*radpsd(:,2))).*effrad.^2.*(radpsd(2,1)-radpsd(1,1)))).*sum(((radpsd(:,1)-effrad).^2).*pi.*radpsd(:,1).^2.*radpsd(:,2).*(radpsd(2,1)-radpsd(1,1)));

    Deff=effrad.*2;
    %   pause

    allvol=4./3.*pi.*radpsd(:,1).^3.*radpsd(:,2);


    % SCIENCE ENDS.....or does it?

    save([data_path_save, Sample_name(1:end-4),'_mod.mat'],'Deff','bin_heights','bin_diams', 'effvar', 'ci_vol')   %% also need to get civol out there too.


    %%%%%%%%% PLOTTING %%%%%%%%%

    % f1=figure(1);clf
    % set(f1,'position',[5         5        1276         705])
    % subplot(3,1,1)
    % p1=plot(bin_diams,bin_heights,'r.'); hold on; grid;
    % xlabel('Diameter [um]','fontsize',10)
    % ylabel('Number of cells ','fontsize',10)
    % title([strrep(Sample_name,'_','-'),': Raw Counts']);
    % set(gca,'fontsize',10)
    % 
    % subplot(3,1,2)
    % plot(asd(:,1),psd_sd,'r.'); hold on; grid;
    % plot(tpsd(:,1),tpsd(:,2),'-');
    % xlabel('Diameter [um]','fontsize',10)
    % ylabel('Number of cells ','fontsize',10)
    % title([strrep(Sample_name,'_','-'),': Blank Corrected PSD']);
    % set(gca,'fontsize',10)

    % subplot(3,1,3)
    % plot(bin_diams,allvol,'-'); hold on; grid;
    % title([strrep(Sample_name,'_','-'),': Total Volume of Cells, D_e_f_f = ',num2str(Deff)]);
    % xlabel('Diameter [um]','fontsize',10)
    % ylabel('Total Volume of Cells [um^3/L]','fontsize',10)
    % set(gca,'fontsize',10)

    % print(f1,'-depsc','-painters','-cmyk',[data_path,'FIGURES2/',Sample_name,'_SUMMARY'])

    disp([Sample_name,':',num2str(Deff),':', num2str(effvar)])


    % out0(j,1)= Deff ; out0(j,2)= effvar; out0(j,3)= civol;  names0{j}= Sample_name;
    % list1=list1 ; list2=list2' ; names=names'

    % end


