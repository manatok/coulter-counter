function [binheight,bindiam]=CCsingle_HEK_function(pname,name,count,dilute);

%CCsingle - routine to take a single multisizer file and loop through extracting particle counts
%and bin sizes, ignoring header data. Note: writes raw data out in two columns

%Choose single data file to extract from, and ask for no. counts and dilution
%[name, pname]=uigetfile('*.*', 'Choose a raw multisizer file');
%count=input('How many 2 ml manometer counts? ');
%dilute=input('What sample dilution (1:5=5, blank=0)? ');

% disp('Processing Data....');
tag_diam='[#Bindiam]';
tag_height='[#Binheight]';

%Open file to read
eval(['fid=fopen(''' pname,'/',name ''');'])

%Find out number of rows in file

r=0; x=0;

%Loop through data file until we get a -1 indicating EOF
while(x~=(-1))
    x=fgetl(fid);
    r=r+1;
end
file_length = r-1;

%Show no. of row and rewind file
% disp(['Number of rows = ' num2str(r)])
frewind(fid);

%loop to read data based on tags: each tag is looped for separately

ii=1;
jj=1;

for lineno=1:file_length,                               % < searches through line numbers
    line=fgetl(fid);                                    % < advances files line
    if strcmp(line,tag_diam);                           % < checks if line is tag, enters loop if true
        %disp([line,' found']);                          % < returns tag found
        tag_diam_start = lineno+1;                      % < records line number of next line which is first record
        for record_length=lineno:file_length;           % < performs nested loop to find next '[' character
            line=fgetl(fid);                            % < advances file line
            if line(1)=='['                             % < executes if '[' found
                all_tags_end(ii,1) = record_length;     % < remembers positions of all further '['
                tag_diam_end = all_tags_end(1,1);       % < takes next occurence of '[' as end of tag record
                ii=ii+1;
            else
            end
        end
    end
end

record_length=tag_diam_end-tag_diam_start+1;
% disp([num2str(record_length),' records found']);

clear all_tags_end ii jj 
frewind(fid);                                           % MUST CLEAR LOOP VARIABLES AND REWIND FILE TO SEARCH FOR NEW TAGS

ii=1;
jj=1;

for lineno=1:file_length,
    line=fgetl(fid);
    if strcmp(line,tag_height);
        %disp([line,' found']);
        tag_height_start = lineno+1;
        for record_length=lineno:file_length;
            line=fgetl(fid);
            if line(1)=='['    
                all_tags_end(ii,1) = record_length;
                tag_height_end = all_tags_end(1,1);
                ii=ii+1;
            else
            end
        end
    end
end

record_length=tag_height_end-tag_height_start+1;
% disp([num2str(record_length),' records found']);

%
%  We now have start and end points for each tag; more loops can be added
%  for more tags if required.  Rewind file to read out.
%
clear all_tags_end ii jj 
frewind(fid);

%read file to get tag data

ii=1;
jj=1;

for lineno=1:file_length;
    line=fgetl(fid);
    if lineno >=tag_diam_start && lineno<=tag_diam_end
            bindiam(ii,1)=sscanf(line,'%f')';
            ii=ii+1;
    elseif lineno >=tag_height_start && lineno<=tag_height_end
            binheight(jj,1)=sscanf(line,'%f')';
            jj=jj+1;
    else
    end
end

% %write psd size bin data to 1st column of writepsd
% writepsd(:,1)=psd(:,4);
% 
% %Write psd N data after Median filter data for spike removal NB uses block size of 6
% writepsd(:,2)=medfilt1(psd(:,2),10);
% 
% %Also strip first six data points and last eight, as all these
% %outside the confidence range of the Coulter Counter
% writepsd=writepsd(7:121,:);
% 
% %Calculate in cells/litre
% writepsd(:,2)=writepsd(:,2).*(1000/(2*count));
% 
% % 	%write single psd values to "name".psd
% % 	writename=strrep(name,'psr','raw');
% % 	writename=strrep(writename,'psb','raw');
% % %	eval(['save ' writename ' writepsd -ascii'])

fclose(fid);

