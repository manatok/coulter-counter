function mkdirIfNotExists(path)
    if ~exist(path, 'dir')
        mkdir(path);
        addpath(path);
    end
end