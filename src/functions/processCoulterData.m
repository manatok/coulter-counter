function processCoulterData(config, isCTD)

    if (isCTD == 1)
        suffix = 'ctd/';
    else
        suffix = 'uw/';
    end
    
    sampleDir = [config.samplesDir suffix];
    outputDir = [config.processedSamplesDir suffix];
    
    dateFolders = dir(sampleDir);
    
    for i = 1:length(dateFolders)
        % skip . and ..
        if ( ~validPath(dateFolders(i).name))
            continue;
        end
        
        dateFolder = [sampleDir dateFolders(i).name];
        stationFolders = dir(dateFolder);
        
        for j = 1:length(stationFolders)
            % skip . and ..
            if ( ~validPath(stationFolders(j).name))
                continue;
            end
            
            stationFolder = [dateFolder '/' stationFolders(j).name];
            
            if (isCTD == 1)
                
                niskinFolder = dir(stationFolder);
                
                for k = 1:length(niskinFolder)
                    if ( ~validPath(niskinFolder(k).name))
                        continue;
                    end
                    processSamples(config, [stationFolder '/' niskinFolder(k).name '/' ], outputDir, isCTD);
                end
            else
                processSamples(config, [stationFolder '/'], outputDir, isCTD)
            end
        end
    end
end