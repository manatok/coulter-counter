function ProcessBlanks(blanksPath, outputDir, isCTD)
    % @param blanksPath string (The blanks directory)
    % @param outputDir string (The blanks processed directory)
    % @param isCTD int  (0 = UW; 1 = CTD)
    
    if (isCTD == 1)
        suffix = 'ctd/';
    else
        suffix = 'uw/';
    end
    
    blanksPath = [blanksPath suffix];
    outputDir = [outputDir suffix];
    
    file=dir(strcat(blanksPath,'*.m4'));
    disp([num2str(length(file)),' files found']);
    

    tmpJDay = -1;
    tmpEvent = -1;
    n = 1;


    for j=1:size(file)

       name = file(j).name;

       if (j == 1)
          prevName = name;
       end


       disp(strcat('Processing File....: ', name));
%        display(['TmpJDay: ' tmpJDay 'newJDay' name(1:3)]);
%        display(['TmpJDay: ' tmpEvent 'actualEvent' name(5:8)]);
   
       
       if (strcmp(tmpJDay,name(1:3)) == 0 || strcmp(tmpEvent,name(5:8))==0)
            display('Changed!!!');
            date = (prevName(1:3));
            event = (prevName(5:8));
            aperture = str2num(prevName(13:15));
            station = (prevName(17:21));
            prevName = name;

            n = 1;

            if (tmpJDay ~= -1)
                saveBlankMean(outputDir, Allbindiam, Allbinheight, date, event, aperture, station);
                clearvars Allbinheight Allbindiam
            end

            tmpJDay = name(1:3);
            tmpEvent = name(5:8);

       end

       [binheight,bindiam]=CCsingle_HEK_function_working(blanksPath, name,1,1);
       Allbinheight(n,:)=binheight;
       Allbindiam(n,:)=bindiam;
       n = n + 1;
    end
end