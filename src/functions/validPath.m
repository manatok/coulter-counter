function [isValid] = validPath(path)
    if (strcmp(path, '.') == 1 || strcmp(path,'..') == 1 ||  strcmp(path(1),'.') == 1)
        isValid = 0;
    else
        isValid = 1;
    end
end