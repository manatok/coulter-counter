function saveBlankMean(output_dir, Allbindiam, Allbinheight, date, event,aperture, station )
    
    Allbindiam = mean(Allbindiam);
    Allbinheight = mean(medfilt1(Allbinheight));

    blank(:,1) = Allbindiam; 
    blank(:,2) = Allbinheight;

    %create file name and output directory
    fname = getFileName('blank_', date, event, aperture, station,'');
    
    mkdirIfNotExists(output_dir);
    
    f=fopen([output_dir,fname],'wt');
    fprintf(f,'%d %d',blank);
    fclose(f);

    save([output_dir,fname],'blank'); 
    disp(' data processed and saved....');
    
end
