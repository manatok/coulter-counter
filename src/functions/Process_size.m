clear all
clear clc



addpath ('/Users/tjs305/Documents/MATLAB/recoultercounterscripts/processed files/');
pname= '/Users/tjs305/Documents/MATLAB/recoultercounterscripts/processed files/';

% Call all files in a folder
file=dir('*.m4');


disp([num2str(length(file)),' files found'])
count=length(file)


%loop through each file extracting relevant bindiameters (ESD)
%and binheights (abundance).
for j=1:size(file)

name=file(j).name
disp('Processing Data....');
                
     date=str2num(name(7:12));
     aperture=str2num(name(14:16));
     %depth=str2num(name(15));


    % Call the function 
    [binheight,bindiam]=CCsingle_HEK_function_working(pname, name,1,1);
    Allbinheight(j,:)=binheight;
    Allbindiam(j,:)=bindiam ;
    
    % Remove Blank option 1:
% interactively with a pop up window to chose each file

% blank= uigetfile('*.*', 'Choose a blank')
 

% Remove blank option 2: 
% find matching blank to current file name. 
 blankname= ['blank_', num2str(date), '_', num2str(aperture), '.mat'];
 path= '/Users/tjs305/Documents/MATLAB/recoultercounterscripts/processed blanks/'

 blank = [path, blankname]
 load(blank)

%remove the blank from that day 
bindata(:,1)= mean(Allbindiam)'; bindata(:,2)=sum(medfilt1(Allbinheight))'- blank(:,2)   ;  
 
% calclate cell volume assuming sphericity
bindata(:,3)=4/3.*pi.*bindata(:,1)/2.^3 ;  

%calculate cells/per litre 
bindata(:,4)=bindata(:,2).*(1000/(2*count));


%remove negative numbers
bindata(bindata <= 0) =0;
%remove data out of confidence limit
bindata=bindata(7:395,:);
    
%% TO SAVE
%create file name and stipulate output path  
fname=['W2015_',num2str(date),'_',num2str(aperture),'_',num2str(depth)]
output_dir = '/Users/tjs305/Documents/MATLAB/recoultercounterscripts/processed files/'
   
%Save file with 4 columns, original data, cell volume and cells per litre
f=fopen(fname,'wt');
fprintf(f,'%d %d',bindata);
fclose(f);;    
save([output_dir,fname],'bindata');
    
    
    
    
    
end


%% SECOND PROCESSING STEP. This is to remove detrital distributions,
%% calculate effective diameter, effective variance and ci volumes.
%% When removing detritus, assumptions are made that detritus follows a
%% jungian slope of -4. 


                    
                    
                    
                    
                    
                    
                    