function processSamples(config, sampleDir, outputDir, isCTD)
     
    if (isCTD == 1)
        pathPositions = config.ctdfile;
    else
        pathPositions = config.uwfile;
    end
    
    blankDir = [config.processedBlankDir 'uw/'];

    file=dir(strcat(sampleDir,'*.m4'));
    disp([num2str(length(file)),' files found']);
    
    if (isempty(file))
        warning(['No files found in: ' sampleDir]);
        return;
    end
   
    for j=1:size(file)
        name = file(j).name;
        display(strcat('Going to process file: ', name));
        date = (name(pathPositions.dateStart:pathPositions.dateEnd));
        event = (name(pathPositions.eventStart:pathPositions.eventEnd));
        aperture = str2num(name(pathPositions.apertureStart:pathPositions.apertureEnd));
        station = (name(pathPositions.stationStart:pathPositions.stationEnd));
        
        [binheight,bindiam] = CCsingle_HEK_function_working(sampleDir, name,1,1);
        Allbinheight(j,:) = binheight;
        Allbindiam(j,:) = bindiam ;
    end
 
    fname = [config.cruisePrefix,'_',char(date),'_',num2str(event),'_',num2str(aperture),'_',char(station)];
 
    blank = loadBlank(blankDir, config.averageBlank, date, event, aperture, 'BLANK');
 
    blank2use = blank(:,2); 
    binh=mean(medfilt1(Allbinheight))';
    % CHECK IF MEDFILT AFTER BLA?K OR NOT 
    %find dilution, if any
    [dils, names]= xlsread(config.dilutionPath);
    ind = find(ismember(names, fname)==1);
   
    if (size(ind) > 0)
        dilution = dils(ind(1));
    else
        dilution = 1;
    end
    
    %create arrays of data remove the blank from that day
    bindata(:,1) = mean(Allbindiam)'; 
    bindata(:,2) = binh - blank2use;

    %and cell volume assuming sphericity
    bindata(:,3) = 4/3.*pi.*(bindata(:,1)/2).^3 ;  % just double check this might be wrong syntax - might need to bracekt ^3.

    %calculate cells/per m3
    bindata(:,4) = bindata(:,2).*((1000000/2).*dilution);

    %remove negative numbers  %remove data out of confidence limit
    bindata(bindata <= 0) = 0; 
    bindata = bindata(7:395,:);
    
    mkdirIfNotExists(outputDir);
    
    csvwrite([outputDir fname '.csv'],  bindata(:,1:4));
    
    save([outputDir fname],'bindata'); 
    disp('data processed and saved....');
    display(fname);
    

end