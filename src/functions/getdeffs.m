function getdeffs(config, isCTD,detritus)
    if nargin < 3
        detritus = 0;
    end

    if (isCTD == 1)
        suffix = 'ctd/';
        outputfile = 'output_ctd.txt';
    else
        suffix = 'uw/';
        outputfile = 'output_uw.txt';
    end
    
    processedSamplesDir = [config.processedSamplesDir suffix];
    
    file = dir(strcat(processedSamplesDir,'*.mat'));
    
    %Out = zeros(length(file),4);
    Out = cell(length(file), 4);
    
    for j=1:size(file)

        Sample_name=file(j).name;

        [Sample_name,Deff, effvar, ci_vol]= Calculate_Effective_Diameter(processedSamplesDir,Sample_name,detritus);

        Out{j,1} = Sample_name;
        Out{j,2}= Deff ; 
        Out{j,3}= effvar ; 
        Out{j,4}= ci_vol ;
    end

    T = cell2table(Out, 'VariableNames', {'Filename', 'Deff', 'effvar', 'ci_vol'});
    if detritus == 1
        outputfile = strcat('minus_detritus_',outputfile);
    end
    writetable(T, outputfile);
    %save(outputfile,'-ascii','Out')
end