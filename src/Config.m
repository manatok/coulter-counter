function [config] = Config()
    config = [];
    
    scriptName = mfilename('fullpath');
    [currentpath, ~, ~] = fileparts(scriptName);
    
    config.cruisePrefix = 'ACE';
    
    config.dataDir          = [currentpath '/../data/'];
    config.blanksDir        = [config.dataDir 'blanks/'];
    config.samplesDir       = [config.dataDir 'samples/'];
    config.dilutionPath     = [config.dataDir  '/../data/dilutions.xls'];
    config.processedDir     = [currentpath '/../processed/'];
    config.processedBlankDir = [config.processedDir 'blanks/'];
    config.processedSamplesDir = [config.processedDir 'samples/'];
    
    config.averageBlank = [config.processedBlankDir 'uw/blank_001_0150_100_BLANK.mat'];
    %config.averageBlank = [config.processedBlankDir 'uw/'];

    config.blankfile = [];
    config.blankfile.dateStart = 1;
    config.blankfile.dateEnd = 3;
    config.blankfile.eventStart = 5;
    config.blankfile.eventEnd = 8;
    config.blankfile.apertureStart = 13;
    config.blankfile.apertureEnd = 15;
    config.blankfile.stationStart = 17;
    config.blankfile.stationEnd = 21;
    
    config.uwfile = [];
    config.uwfile.dateStart = 1;
    config.uwfile.dateEnd = 3;
    config.uwfile.eventStart = 5;
    config.uwfile.eventEnd = 8;
    config.uwfile.apertureStart = 13;
    config.uwfile.apertureEnd = 15;
    config.uwfile.stationStart = 17;
    config.uwfile.stationEnd = 21;
    
    config.ctdfile = [];
    config.ctdfile.dateStart = 1;
    config.ctdfile.dateEnd = 3;
    config.ctdfile.eventStart = 5;
    config.ctdfile.eventEnd = 8;
    config.ctdfile.apertureStart = 13;
    config.ctdfile.apertureEnd = 15;
    config.ctdfile.stationStart = 17;
    config.ctdfile.stationEnd = 22;
    