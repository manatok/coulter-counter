function fileName = getFileName(prefix, date, event, aperture, station, suffix)
    fileName = [prefix, char(date),'_',char(event),'_',num2str(aperture),'_',char(station),suffix];
end