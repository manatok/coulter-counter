function blank = loadBlank(path, aveBlank, date, event, aperture, station)
 
    blankname = getFileName('blank_', date, event, aperture, station, '.mat');
    display('Looking for blank');
    display(blankname);
    
    if exist([path blankname], 'file')
        blankfile = [path blankname];
    else
        display(['Could not find a blank by the name of: ' blankname]);
        
        blanks=dir(strcat(path,'*.mat'));
        
        minDist = 1000;
        
        for i=1:size(blanks)
            parts = strsplit(blanks(i).name, '_');
            jday = str2num(cell2mat(parts(2)));
           
           
            if (abs(str2num(date) - jday) < minDist)
                minDist = abs(str2num(date) - jday);
                blankfile = blanks(i).name;
            end
        end
        display(['Found the next best blank: ' blankfile]);
        
        % We could not find any blanks in this folder
        if (minDist == 1000) 
            blankfile = aveBlank;
        end
    end
    
    load(blankfile, 'blank');
end