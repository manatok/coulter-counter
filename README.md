# README #

A set of MATLAB scripts used for processing the Coulter counter data.

### High-level usage ###

* Clone out this codebase
* Add the output files from the Coulter to the data dir (Folder structure explained below)
* Open the project in MATLAB
* Make sure all the folders are added to MATLAB's path (Right click on folder, "Add To Path -> Selected Folders and Subfolders"
* Open up the src/Config.m file and make sure everything is correct (Explained below)
* Run the src/Run.m file

### Data Folder Structure ###

It is very important that the files are placed in the following folder structure:

#### Blanks ####
All blanks should be placed in the data/blanks/uw/ folder. At this stage there is no differentiation between blanks for CTD's and ones for underway samples. Depending on how you have named the files you might have to update the Config.m file. 

The default naming convention is:

JDAY_EVENT_SCIENTIST_APERTURE_BLANK_# e.g 001_0149_DA_100_BLANK_01_1

#### UW Samples ####
All underway samples live in the data/samples/uw folder with the following folder structure:

jday/UW#/Samples

#### CTD Samples ####
All CTD samples live in the data/samples/ctd folder with the following folder structure:

jday/Station/Niskin/Samples